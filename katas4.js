const gotCitiesCSV = "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen";
const lotrCitiesArray = ["Mordor","Gondor","Rohan","Beleriand","Mirkwood","Dead Marshes","Rhun","Harad"];
const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit";

function kata1(){
    return ["Kata One", JSON.stringify(gotCitiesCSV.split(","))]
}

function kata2(){
    return ["Kata Two", JSON.stringify(bestThing.split(" "))]
}

function kata3(){
    return ["Kata Three", JSON.stringify(gotCitiesCSV.split(",").join("; "))]
}

function kata4(){
    return ["Kata Four", JSON.stringify(lotrCitiesArray.join(", "))]
}

function kata5(){
    return ["Kata Five", JSON.stringify(lotrCitiesArray.slice(0, 5))]
}

function kata6(){
    return ["Kata Six", JSON.stringify(lotrCitiesArray.slice(lotrCitiesArray.length - 5, lotrCitiesArray.length))]
}

function kata7(){
    return ["Kata Seven", JSON.stringify(lotrCitiesArray.slice(2, 5))]
}

function kata8(){
    let temp = lotrCitiesArray
    temp.splice(2, 1)
    return ["Kata Eight", JSON.stringify(lotrCitiesArray)]
}

function kata9(){
    let temp = lotrCitiesArray
    temp.splice(5, 2)
    return ["Kata Nine", JSON.stringify(lotrCitiesArray)]
}

function kata10(){
    let temp = lotrCitiesArray
    temp.splice(2, 0, "Rohan")
    return ["Kata Ten", JSON.stringify(lotrCitiesArray)]
}

function kata11(){
    let temp = lotrCitiesArray
    temp.splice(5, 1, "Deadest Marshes")
    return ["Kata Eleven", JSON.stringify(lotrCitiesArray)]
}

function kata12(){
    return ["Kata Twelve", JSON.stringify(bestThing.slice(0, 14))]
}

function kata13(){
    return ["Kata Thirteen", JSON.stringify(bestThing.slice(bestThing.length - 12, bestThing.length))]
}

function kata14(){
    return ["Kata Fourteen", JSON.stringify(bestThing.slice(23, 38))]
}

function kata15(){
    return ["Kata Fifteen", JSON.stringify(bestThing.substring(bestThing.length - 12, bestThing.length))]
}

function kata16(){
    return ["Kata Sixteen", JSON.stringify(bestThing.substring(23, 38))]
}

function kata17(){
    return ["Kata Seventeen", bestThing.indexOf("only")]
}

function kata18(){
    let temp = bestThing.split(" ")
    let lastWord = temp[temp.length - 1]
    return ["Kata Eighteen", bestThing.indexOf(lastWord)]
}

function kata19() {
    let checkCity = (city) => {
        if(city.includes("aa") || city.includes("ee") || city.includes("ii") || city.includes("oo") || city.includes("uu")){
            return city
        }
    }
    return ["Kata Nineteen", JSON.stringify(gotCitiesCSV.split(",").filter(checkCity))]
}

function kata20(){
    let checkCity = (city) => {
        if(city.slice(city.length - 2, city.length) === "or"){
            return city
        }
    }
    return ["Kata Twenty", JSON.stringify(lotrCitiesArray.filter(checkCity))]
}

function kata21(){
    let checkWord = (word) => {
        if(word[0] === "b"){
            return word
        }
    }
    return ["Kata Twentyone", JSON.stringify(bestThing.split(" ").filter(checkWord))]
}

function kata22() {
    let output = "No"
    if(lotrCitiesArray.includes('Mirkwood')){
        output = "Yes"
    }
    return ["Kata Twentytwo", JSON.stringify(output)]
}

function kata23() {
    let output = "No"
    if(lotrCitiesArray.includes('Hollywood')){
        output = "Yes"
    }
    return ["Kata Twentythree", JSON.stringify(output)]
}

function kata24(){
    return ["Kata Twentyfour", lotrCitiesArray.indexOf("Mirkwood")]
}

function kata25(){
    let findCity = (city) => {
        if(city.includes(" ")){
            return city
        }
    }
    return ["Kata Twentyfive", JSON.stringify(lotrCitiesArray.find(findCity))]
}

function kata26(){
    let temp = lotrCitiesArray
    temp.reverse()
    return ["Kata Twentysix", JSON.stringify(temp)]
}

function kata27(){
    let temp = lotrCitiesArray
    temp.sort()
    return ["Kata Twentyseven", JSON.stringify(temp)]
}

function kata28(){
    let sortFilter = (a, b) => {
        return a.length - b.length
    }
    return ["Kata Twentyeight", JSON.stringify(lotrCitiesArray.sort(sortFilter))]
}
let cityHold;
function kata29(){
    cityHold = lotrCitiesArray.pop(lotrCitiesArray.length - 1)
    return ["Kata Twentynine", JSON.stringify(lotrCitiesArray)]
}

function kata30(){
    lotrCitiesArray.push(cityHold)
    cityHold = null
    return ["Kata Thirty", JSON.stringify(lotrCitiesArray)]
}

function kata31(){
    cityHold = lotrCitiesArray.shift()
    return ["Kata Thirtyone", JSON.stringify(lotrCitiesArray)]
}

function kata32(){
    lotrCitiesArray.unshift(cityHold)
    cityHold = null
    return ["Kata Thirtytwo", JSON.stringify(lotrCitiesArray)]
}
// DOM Stuff //
let functions = [kata1(), kata2(), kata3(), kata4(), kata5(), kata6(), kata7(), kata8(), kata9(), kata10(), kata11(), kata12(), kata13(), kata14(), kata15(), kata16(), kata17(), kata18(), kata19(), kata20(), kata21(), kata22(), kata23(), kata24(), kata25(), kata26(), kata27(), kata28(), kata29(), kata30(), kata31(), kata32()]
wrapper = document.createElement('div')
wrapper.className = "content"
for(let i = 0; i < functions.length; i++){
    let kataDiv = document.createElement('div')
    kataDiv.className = "kata"

    let title = document.createElement('h1')
    let titleText = document.createTextNode(functions[i][0]);
    title.appendChild(titleText);

    let content = document.createElement('P')
    let contentText = document.createTextNode(functions[i][1])
    content.appendChild(contentText)

    kataDiv.appendChild(title)
    kataDiv.appendChild(contentText)
    wrapper.appendChild(kataDiv)
}
document.getElementById('root').appendChild(wrapper)